#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <fts.h>
#include <string.h>
#include <errno.h>
#include <iconv.h>

int isone(unsigned char *str) {
	while (*str) {
		if (*str >= 0x80) {
			// 2-byte sequence
			if ((*str & 0xE0) == 0xC0) {
				if ((*++str & 0xC0) == 0x80) {
					++str;
					continue;
				}
			}
			// 3-byte sequence
			if ((*str & 0xF0) == 0xE0) {
				if ((*++str & 0xC0) == 0x80) {
					if ((*++str & 0xC0) == 0x80) {
						++str;
						continue;
					}
				}
			}
			// 4-byte sequence
			if ((*str & 0xF8) == 0xF0) {
				if ((*++str & 0xC0) == 0x80) {
					if ((*++str & 0xC0) == 0x80) {
						if ((*++str & 0xC0) == 0x80) {
							++str;
							continue;
						}
					}
				}
			}
			return 1;
		}
		str++;
	}
	return 0;
}

void escprint(unsigned char *str, int hi) {
	while (*str) {
		switch (*str) {
			case '\'':
			case '\\':
				printf("\\%.1s", str);
				break;
			default:
				if (!hi && *str >= 0x80)
					printf("\\x%2X", *str);
				else printf("%.1s", str);
		}
		++str;
	}
}


#define MAXDEPTH 512

char tree[MAXDEPTH][4096 * 2];

int main(int argc, char *argv[]) {
	FTS *fts = NULL;
	FTSENT *node = NULL;
	iconv_t icv;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <path>\n", argv[0]);
		return 1;
	}

	icv = iconv_open("UTF8", "cp866");
	if (icv == (iconv_t) -1) {
		fprintf(stderr, "error: iconv error\n");
		return 1;
	}
	fts = fts_open(argv + 1, FTS_COMFOLLOW | FTS_NOCHDIR, NULL);
	if (fts != NULL) {
		while((node = fts_read(fts)) != NULL) {
			char *inbuf, *outbuf;
			size_t inlen, outlen;
			
			switch (node->fts_info) {
				case FTS_D:
				case FTS_F:
				case FTS_SL:
					if (node->fts_level >= MAXDEPTH - 1) {
						fprintf(stderr, "error: path too deep\n");
						return 1;
					}
					if (isone(node->fts_name)) {
						inbuf = node->fts_name;
						outbuf = tree[node->fts_level];
						inlen = strlen(inbuf);
						outlen = sizeof(tree[node->fts_level]);
						if (iconv(icv, &inbuf, &inlen, &outbuf, &outlen) == (size_t) -1) {
							fprintf(stderr, "error: name too long\n");
							return 1;
						}
						*outbuf = 0;
						
						printf("mv $'");
						for (int i = 0; i < node->fts_level; ++i) {
							escprint(tree[i], 1);
							printf("/");
						}
						escprint(node->fts_name, 0);
						
						printf("' $'");
						for (int i = 0; i < node->fts_level; ++i) {
							escprint(tree[i], 1);
							printf("/");
						}
						escprint(tree[node->fts_level], 1);
						printf("'\n");
					} else {
						if (strlen(node->fts_name) >= sizeof(tree[node->fts_level])) {
							fprintf(stderr, "error: name too long\n");
							return 1;
						}
						strcpy(tree[node->fts_level], node->fts_name);
					}
					break;
				default:
					break;
			}
		}
		fts_close(fts);
	}
	iconv_close(icv);

	return 0;
}
